﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scalers : MonoBehaviour {
    double scale=1;
    Vector3 scale2 = Vector3.one;
    float PosY = 0;
    float PosZ = 0;
    float PosX = 0;
    public GameObject toBeScaled;
    public GameObject toBeScaled2;
    public GameObject toBeRised;
    public UnityEngine.UI.Text debug;
	// Use this for initialization
	void Start ()
    {
        PosX = toBeRised.transform.position.x;
        PosY = toBeRised.transform.position.y;
        PosZ = toBeRised.transform.position.z;
        scale = toBeScaled.transform.localScale.x;
        scale2 = toBeScaled2.transform.localScale;
    }
	
	// Update is called once per frame
	void Update () {

        PosX = toBeRised.transform.position.x;
        PosY = toBeRised.transform.position.y;
        PosZ = toBeRised.transform.position.z;
        if (!OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) && !Input.GetKey(KeyCode.Space))
        {
            if (OVRInput.Get(OVRInput.Button.PrimaryTouchpad))
                scale +=OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).y*0.001f;
            if (!OVRInput.Get(OVRInput.Button.PrimaryTouchpad))
                PosZ += OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).y*0.01f;
            scale += Input.GetAxis("Horizontal");
            
        }
        /*if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) || Input.GetKey(KeyCode.Space))
        {

            if (!OVRInput.Get(OVRInput.Button.PrimaryTouchpad))
                scale2.x += OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).x;
            if (OVRInput.Get(OVRInput.Button.PrimaryTouchpad))
                scale2.z += OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).y;
            //PosX += OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).x*0.5f;
            scale2.x += Input.GetAxis("Horizontal");
            if (scale2.x < 1)
                scale2.x = 1;
            if (scale2.z < 1)
                scale2.z = 1;
        }*/
        toBeScaled.transform.localScale = Vector3.one * (float)scale;
        //toBeScaled2.transform.localScale = scale2;

        ///
        if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) || Input.GetAxis("Vertical") != 0)
        {

            /*if (OVRInput.Get(OVRInput.Button.PrimaryTouchpad))
                PosZ += OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).y * 0.1f;*/
            //else
            //{
                PosY += OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).y * 0.01f;
                //PosX += OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).x * 0.1f;
            //}
            //height += Input.GetAxis("Vertical");
            if (PosY < 1)
                PosY = 1;
           /* if (depth < 1)
                depth = 1;*/
        }

        toBeRised.transform.position=new Vector3(PosX, PosY, PosZ);
        debug.text = "S: " + scale +
            ", X: " + PosX +
            ", Y: " + PosY +
            ", Z: " + PosZ;
       // debug.text = "S: " + scale +
        //    "S2: " + scale2;
    }
}
