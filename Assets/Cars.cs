﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cars : MonoBehaviour {
    public GameObject[] carsPrefabs;
    public int selectedIndex;
    public MyNetworkManager NetworkManager;
    public NetworkUI networkUI;
    bool nextOrPrevious=true;
    public int SelectedIndex
    {
        get
        {
            return selectedIndex;
        }

        set
        {

            selectedIndex = value;
        }
    }

    // Use this for initialization
    void Start () {

        foreach(var i in carsPrefabs)
        {

            foreach(var J in i.GetComponents<MonoBehaviour>())
            {
                Destroy(J);
            }

            i.SetActive(false);
            Destroy(i.GetComponent<UnityEngine.Networking.NetworkIdentity>());
        }

        SelectedIndex = carsPrefabs.Length - 1 ;
        selectedIndex = 100;
        NextCar();
        readyToChange = 1;
    }
	
    public void NextCar()
    {
        
        RTC = 1;
        if (carsPrefabs.Length <= 0)
            return;
            if (selectedIndex < 0)
                selectedIndex = carsPrefabs.Length-1;
            if (selectedIndex >= carsPrefabs.Length)
                selectedIndex %= carsPrefabs.Length;

            carsPrefabs[selectedIndex].SetActive(false);
        
        if (nextOrPrevious)
        {    selectedIndex++;
            selectedIndex %= carsPrefabs.Length;
        }else
        {
            selectedIndex--;
            if (selectedIndex < 0)
                selectedIndex = carsPrefabs.Length - 1;
        }
            carsPrefabs[selectedIndex].SetActive(true);
            NetworkManager.playerIndex = selectedIndex;
            networkUI.PlayerIndex = selectedIndex;
    }
    public int RTC;
    public int readyToChange { set { RTC = value; ; } get { return RTC; } }
    private void Update()
    {
        //Debug.Log(Input.GetAxis("Vertical"));
        //Debug.Log("Ready "+readyToChange);
        if ((OVRInput.GetDown(OVRInput.Button.One) && Mathf.Abs(OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).x)>.5f || Mathf.Abs(Input.GetAxis("Vertical")) > 0 ) && readyToChange == 1)
        {
            nextOrPrevious = true;
            if (OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).x < 0|| Input.GetAxis("Vertical")<0)
                nextOrPrevious = false;
            Debug.Log("3AAAh");
            GetComponent<Animator>().SetTrigger("Change");
            GetComponent<Animator>().SetTrigger("Disappear");
        }
    }
}
