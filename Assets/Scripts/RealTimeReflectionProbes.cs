﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RealTimeReflectionProbes : MonoBehaviour {
    ReflectionProbe reflectionProbe;
    public int refreshRate;
    public float lastTime;
    // Use this for initialization
    void Start () {
        reflectionProbe = GetComponent<ReflectionProbe>();     
	}

    // Update is called once per frame
    void Update() {
        if (Time.time > (lastTime + 1 / refreshRate))
        {
            lastTime = Time.time;
            reflectionProbe.RenderProbe();
        }
    }
}
