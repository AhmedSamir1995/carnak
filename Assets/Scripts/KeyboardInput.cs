﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : MotionVectorsInput {

   
    public override void getInput()
    {

        TorqueAndSteering = new Vector2(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
        Brake = Input.GetKey(KeyCode.Space);

    }
}
