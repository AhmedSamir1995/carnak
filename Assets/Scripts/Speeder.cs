﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(BoxCollider))]

public class Speeder : MonoBehaviour {
    public float ForceMultiplier=20;
    public Vector3 Direction;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerStay(Collider other)
    {

        if (other.attachedRigidbody.tag == "Player")
        {
            other.attachedRigidbody.AddForce(Direction * ForceMultiplier);
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        
    }
}
