﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MotionVectorsInput
{
    /// <summary>
    /// This Vector2 contains Torque as X and Steering as Y
    /// </summary>
    public Vector2 TorqueAndSteering;
    public bool Brake;
    public abstract void getInput();

    /// <summary>
    /// 
    /// </summary>
    /// <param name="VehicleToControl"></param>
    public MotionVectorsInput()
    {
        TorqueAndSteering = Vector2.zero;
        Brake = false;
    }
    

    //protected void Update()
    //{
    //    getInput();
    //}
}
