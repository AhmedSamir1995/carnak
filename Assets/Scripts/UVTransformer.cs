﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UVTransformer : MonoBehaviour {



    public MeshFilter meshfilter;
    Mesh mesh;

    public Vector2 offset = Vector2.zero;
    Vector2 oldOffset;

    [Range(-180f, 180f)]
    public float rotation;
    float oldRotation = 0f;

    public Vector2 tiling = new Vector2(1, 1);
    Vector2 oldTiling;

    public float scale = 1;
    float oldScale;

    Vector2[] originalUV;

    private void Awake()
    {
        if (!meshfilter)
            meshfilter = GetComponent<MeshFilter>();
        mesh = meshfilter.mesh;
        originalUV = mesh.uv;
        if (scale == 0)
            scale = 1;
        ApplyUVTransform();
    }

    // Update is called once per frame
    void Update()
    {
        if (oldRotation != rotation || oldScale != scale || oldOffset != offset || oldTiling != tiling)
        {
            ApplyUVTransform();

        }
    }
    public static Vector2 Rotate(Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    public void saveMesh()
    {

    }

    public void ApplyUVTransform()
    {
        Vector2[] temp = new Vector2[originalUV.Length];

        for (int i = 0; i < originalUV.Length; i++)
        {
            //print(originalUV[i] + " " + Rotate(originalUV[i], rotation));
            temp[i] = originalUV[i] + offset;
            temp[i] = Rotate(temp[i], rotation);
            temp[i] = temp[i].multiply(tiling);
            temp[i] *= scale;

        }

        meshfilter.mesh.SetUVs(0, new List<Vector2>(temp));
        oldRotation = rotation;
        oldScale = scale;
        oldOffset = offset;
        oldTiling = tiling;



    }



}

public static class Vector2Extension
{

    public static Vector2 multiply( this Vector2 a, Vector2 b)
    {

        return new Vector2(a.x * b.x, a.y * b.y);
    }
}

