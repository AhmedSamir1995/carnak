﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCam : MonoBehaviour {

    float Delta;
    public Transform Car;
    private void Start()
    {
        Delta = transform.rotation.eulerAngles.y - Car.rotation.eulerAngles.y;
    }
    private void Update()
    {
        transform.rotation = Quaternion.Euler(Vector3.up * (Car.rotation.eulerAngles.y + Delta));
    }
}
