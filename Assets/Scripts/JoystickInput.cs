﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickInput : MotionVectorsInput {
    Vector2 TS;
    //float step = 0.1f;
    public override void getInput()
    {
        TS = new Vector2(-Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")*0.5f);
        TorqueAndSteering.x = TS.x;

        /*if (TS.y == 0)
        {
            if (Mathf.Abs(TorqueAndSteering.y) - 0.05 > 0)
            {
                if (TorqueAndSteering.y > 0)
                    TorqueAndSteering.y -= step;
                else
              if (TorqueAndSteering.y < 0)
                    TorqueAndSteering.y += step;
            }

        }
        else
        {
            if (TS.y > 0 && TorqueAndSteering.y < 1)
                TorqueAndSteering.y += step;
            else
          if (TS.y < 0 && TorqueAndSteering.y > -1f)
                TorqueAndSteering.y -= step;
        }*/
        TorqueAndSteering.y = TS.y;

        //TorqueAndSteering = new Vector2(-Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        Debug.Log("Torque and steering"+TorqueAndSteering);
        Brake = Input.GetKey(KeyCode.Joystick1Button0)|| Input.GetKey(KeyCode.Joystick1Button3)|| Input.GetKey(KeyCode.Joystick1Button4)|| Input.GetKey(KeyCode.Joystick1Button5);
        //DebugInput.Instance.Debug(TorqueAndSteering + " " + Brake);
        //Debug.Log("Torque and steering " + TorqueAndSteering.y);
        //Debug.Log("TS " + TS.y);
    }
    
}
