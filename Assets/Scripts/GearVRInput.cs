﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("CarInput/GearVrController")]
public class GearVRInput : MotionVectorsInput
{
    public float Sensitivity=0.001f;

    public float TiltFactor = .7f;
    public override void getInput()
    {
        
        if (OVRInput.Get(OVRInput.Button.PrimaryTouchpad))
        {
            Vector2 Temp = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad);

            if (Temp.magnitude > Sensitivity && OVRInput.Get(OVRInput.Button.PrimaryTouchpad)) 
            {
                if (OVRInput.IsControllerConnected(OVRInput.Controller.LTrackedRemote) || OVRInput.IsControllerConnected(OVRInput.Controller.RTrackedRemote))
                {
                    Brake = false;
                    float swapper = Temp.x;
                    Temp.x = Temp.y;
                    Temp.y = swapper*TiltFactor;
                    TorqueAndSteering = Temp;
                }
                else
                {
                    Brake = false;
                    Temp.y *= -1;
                    TorqueAndSteering = Temp;
                }
            }
            
        }
        else
        {
            TorqueAndSteering = Vector2.zero;
            Brake = false;
        }
        if (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger))
            {
                TorqueAndSteering = Vector2.right;
                Brake = true;
            }
        if(DebugInput.Instance)
            DebugInput.Instance.Debug(TorqueAndSteering + " + " + Brake);
    }
}
