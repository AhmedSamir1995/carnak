﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureAnim : MonoBehaviour {
    public Vector2 Speed;
    Material myMat;
    Vector2 offset;
	// Use this for initialization
	void Start () {
        myMat = GetComponent<Renderer>().sharedMaterial;

	}
	
	// Update is called once per frame
	void Update () {
        offset += Speed * Time.deltaTime;
        if (offset.x > 10)
            offset.x -= 10;
        if (offset.y > 10)
            offset.y -= 10;

        myMat.SetTextureOffset("_MainTex", offset);
	}
}
