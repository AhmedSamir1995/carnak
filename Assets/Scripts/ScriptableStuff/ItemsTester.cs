﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsTester : MonoBehaviour {
    public int itemId;
	// Use this for initialization
	void Start () {
        ItemsDatabase<Item> itemsDatabase = new ItemsDatabase<Item>();
        Item item1 =itemsDatabase.GetItem(itemId);

        item1.itemName = "CoinEdited";
        if (item1 != null)
            Debug.Log(string.Format("item1 id: {0}, item1 name: {1}, item1 Description: {2}.", item1.Id, item1.itemName, item1.itemDescription));
        else
            Debug.Log("There is no items exist with entered Id");
    }

    // Update is called once per frame
    void Update () {
		
	}
}
