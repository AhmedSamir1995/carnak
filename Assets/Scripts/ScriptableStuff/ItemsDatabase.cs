﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsDatabase <T> where T: Object,IHaveID{

    private List<T> _items;
    private bool isDatabaseLoaded;

    public void ValidateDatabase()
    {
        if (_items == null) _items = new List<T>();
        if (!isDatabaseLoaded) LoadDatabase();
    }

     public void LoadDatabase()
    {
        if (isDatabaseLoaded) return;
        isDatabaseLoaded = true;
        LoadDatabaseForce();
    }
     public void LoadDatabaseForce()
    {
        ValidateDatabase();
        T[] resources = Resources.LoadAll<T>(@"Collectibles");
        
        foreach(var item in resources)
        {
            if(!_items.Contains(item))
            {
                _items.Add(item);
            }
        }
    }

     public void ClearDatabase()
    {
        isDatabaseLoaded = false;
        _items.Clear();
    }

     public T GetItem(int id)
    {
        ValidateDatabase();
        foreach(var item in _items)
        {
            if (item.Id == id)
                return ScriptableObject.Instantiate<T>(item);
        }
        return null;
    }
     public T GetOriginalItem(int id)
    {
        ValidateDatabase();
        foreach (var item in _items)
        {
            if (item.Id == id)
                return item;
        }
        return null;
    }
}
