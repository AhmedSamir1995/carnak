﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : ScriptableObject {

    public string playerName;
    public uint gold;
    public uint wins;
    public uint losses;
}
