﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerdataManager : MonoBehaviour {
    public PlayerData playerData;
    
	// Use this for initialization
	void Start () {
		playerData = Resources.Load<PlayerData>("PlayerData");
        print("player name: " + playerData.playerName);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
