﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHaveID {


    uint Id { get; set; }
}
