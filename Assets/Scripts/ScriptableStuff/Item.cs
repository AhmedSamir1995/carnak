﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : ScriptableObject,IHaveID
{
    uint itemId;
    public string itemName;
    public string itemDescription;

    public uint Id
    {
        get
        {
            return itemId;
        }

        set
        {
            itemId = value;
        }
    }
}
