﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public abstract class Vehicle_Network : NetworkBehaviour {
    public Transform cams;
    public float MaxSpeed;
    public enum Traction
    {
        FourWheelDrive,
        FrontWheelDrive,
        RearWheelDrive
    }
    protected Rigidbody AttachedRigidBody;
    [SyncVar]
    public float Speed;
    public Traction TracktionType;
    protected MotionVectorsInput TorqueSteeringBrake;

    protected virtual void Start()
    {
        if (!isLocalPlayer)
            cams.gameObject.gameObject.SetActive(false);
        if (Application.platform == RuntimePlatform.WindowsPlayer|| Application.platform == RuntimePlatform.WindowsEditor)
            TorqueSteeringBrake = new KeyboardInput();
        else
            
            TorqueSteeringBrake = new GearVRInput();
        AttachedRigidBody = GetComponent<Rigidbody>();
        AttachedRigidBody.centerOfMass = new Vector3(0, -.3f, -.5f);
    }
    
    private void Update()
    {
        if (isLocalPlayer)
        {
            TorqueSteeringBrake.getInput();
            Debug.Log(TorqueSteeringBrake);
        }
        Move();
        UpdateWheelsTransforms();
        CapSpeed();
        Speed = AttachedRigidBody.velocity.magnitude * 60 * 60 / 1000;
        AttachedRigidBody.AddForce(Vector3.down * Speed);
        //DebugInput.Instance.Debug("Speed= " +((int)Speed).ToString()+"\n");
    }

    protected abstract void UpdateWheelsTransforms();

    protected abstract void Move();
    protected abstract void CapSpeed();
}

