﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class MyNetworkManager : NetworkManager {
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Return))
        {
            print("change scene");
            ServerChangeScene("Temple");
        }
    }

 

    public override void OnServerSceneChanged(string sceneName)
    {
        base.OnServerSceneChanged(sceneName);
        
    }
    public GameObject[] PlayerPrefabs;
    [SerializeField]
    public int playerIndex { get; set; }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        //base.OnServerAddPlayer(conn, playerControllerId);
        Transform transform=GetStartPosition();

        GameObject player;
        if (transform)
            player = (GameObject)Instantiate(PlayerPrefabs[playerIndex], transform.position, transform.rotation);
        else
            player = (GameObject)Instantiate(PlayerPrefabs[playerIndex], Vector3.zero, Quaternion.identity);
        print("4445");
        
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);


        
        //ServerChangeScene("Karnak");
    }


    public override void OnServerDisconnect(NetworkConnection conn)
    {
        NetworkServer.DestroyPlayersForConnection(conn);
    }



    public override void OnClientDisconnect(NetworkConnection conn)
    {
        client.Disconnect();
        client.ResetConnectionStats();
        NetworkServer.DestroyPlayersForConnection(conn);
        
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId, NetworkReader extraMessageReader)
    {
        Transform transform = GetStartPosition();
        GameObject player = (GameObject)Instantiate(PlayerPrefabs[extraMessageReader.ReadMessage<IntegerMessage>().value], transform.position, transform.rotation);


        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);



        //base.OnServerAddPlayer(conn, playerControllerId, extraMessageReader);
    }
    public override void OnClientConnect(NetworkConnection conn)
    {
        /// ***
        /// This is added:
        /// First, turn off the canvas...
        /// 
        /// Can't directly send an int variable to 'addPlayer()' so you have to use a message service...
        IntegerMessage msg = new IntegerMessage(playerIndex);
        /// ***

        if (!clientLoadedScene)
        {
            // Ready/AddPlayer is usually triggered by a scene load completing. if no scene was loaded, then Ready/AddPlayer it here instead.
            ClientScene.Ready(conn);
            if (autoCreatePlayer)
            {
                print("adddd");ClientScene.AddPlayer(conn,0, msg);
                ///***
                /// This is changed - the original calls a differnet version of addPlayer
                /// this calls a version that allows a message to be sent
                
            }
            
        }
                

    }
    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        // always become ready.
        ClientScene.Ready(conn);

        /*if (!autoCreatePlayer)
        {
            return;
        }*/

        bool addPlayer = false;
        if (ClientScene.localPlayers.Count == 0)
        {
            // no players exist
            addPlayer = true;
        }

        bool foundPlayer = false;
        foreach (var playerController in ClientScene.localPlayers)
        {
            if (playerController.gameObject != null)
            {
                foundPlayer = true;
                break;
            }
        }
        if (!foundPlayer)
        {
            // there are players, but their game objects have all been deleted
            addPlayer = true;
        }
        if (addPlayer)
        {
        print("NewScene");
            //ClientScene.AddPlayer(0);
            IntegerMessage msg = new IntegerMessage(playerIndex);

            ClientScene.AddPlayer(conn, 0, msg);
        }
        //ClientScene.Ready(conn);
        
    }
}
