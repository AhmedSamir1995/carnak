﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroTest : MonoBehaviour
{

    Vector3 Theta;
    // Use this for initialization
    void Start()
    {
        Debug.Log(SystemInfo.deviceModel);
        if (SystemInfo.supportsGyroscope)
        {
            Input.gyro.enabled = true;
            Theta = Input.gyro.attitude.eulerAngles;
        }
        else
            Debug.Log("Gyroscope is missing");

    }

    // Update is called once per frame
    void Update()
    {

        Vector3 Angle = Input.gyro.attitude.eulerAngles - Theta + new Vector3(360f, 360f, 360f);
        Angle.rem(Angle);
        Angle.x %= 360;
        Angle.y %= 360;
        Angle.z %= 360;
        //transform.rotation = Quaternion.Euler(0, 0, Angle.z);
        transform.Rotate(Vector3.up, Input.gyro.gravity.x);
        Debug.Log(Input.acceleration);
    }
}
static class MyExtensions
{
    public static Vector3 rem(this Vector3 s, Vector3 s2)
    {
        Vector3 temp = Vector3.zero;
        temp.x = s.x % s2.x;
        temp.y = s.y % s2.y;
        temp.z = s.z % s2.z;
        return temp;
    }
}
