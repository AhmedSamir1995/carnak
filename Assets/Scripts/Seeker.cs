﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class Seeker : MonoBehaviour
{
    public Transform Target;
    [Tooltip("KM/H")]
    public float MaxSpeed;
    public float ArrivedDistance;
    Rigidbody AttachedRigidbody;
    public void Start()
    {
        AttachedRigidbody = GetComponent<Rigidbody>();
    }

    protected void Update()
    {
        if (Vector3.Distance(transform.position, Target.position) > ArrivedDistance)
        {
            Seek();
        }
        else
        {
            Stop();
        }
        CapSpeed();
    }
    public void Seek()
    {
        AttachedRigidbody.AddForce((Target.position - transform.position), ForceMode.VelocityChange);
    }
    public void Stop()
    {
        AttachedRigidbody.velocity = Vector3.zero;
    }
    void CapSpeed()
    {
        float MaxSpeed = this.MaxSpeed;
        MaxSpeed *= 3.6f;
        if (AttachedRigidbody.velocity.magnitude >= MaxSpeed)
        {
            AttachedRigidbody.velocity = AttachedRigidbody.velocity.normalized * MaxSpeed;
        }
    }
}
