﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Axle : AxleBase
{

    public WheelType[] Wheels;
    public override void Move(MotionVectorsInput TorqueAndSteer)
    {
        if (HandBrake)
            CheckHandBrakes(TorqueAndSteer.Brake);
        if (PedalBrake)
            CheckPedalBrakes(TorqueAndSteer.TorqueAndSteering.x);
        if (Torque)
            CheckTorque(TorqueAndSteer.TorqueAndSteering.x);
        if (Steer)
            CheckSteering(TorqueAndSteer.TorqueAndSteering.y);
    }

    protected override void CheckHandBrakes(bool Brake)
    {
        if (Brake)
        {
            foreach (var wheel in Wheels)
                wheel.MyCollider.brakeTorque = BrakingTorque;
        }
        else
        {
            foreach (var wheel in Wheels)
                wheel.MyCollider.brakeTorque = 0;
        }
    }
    protected override void CheckPedalBrakes(float Torque)
    {
       // print(Left.MyCollider.motorTorque);
        if (Torque == 0)
        {
            return;
            
        }
        //if (Mathf.Sign(Torque * (invertTorque ? -1 : 1))!=Mathf.Sign(Vector3.Dot(Left.MyCollider.attachedRigidbody.velocity,Left.MyCollider.attachedRigidbody.transform.forward)/ Left.MyCollider.attachedRigidbody.transform.forward.magnitude))
        //{
        //    print("Brakes");
        //    Left.MyCollider.brakeTorque = BrakingTorque;
        //    Right.MyCollider.brakeTorque = BrakingTorque;
        //}
        //else
        //{
        //    Left.MyCollider.brakeTorque = 0;
        //    Right.MyCollider.brakeTorque = 0;
        //}
    }
    protected override void CheckTorque(float Torque)
    {
        foreach (var wheel in Wheels)
            wheel.MyCollider.motorTorque = Torque * MaxTorque * (invertTorque ? -1 : 1);
    }
    protected override void CheckSteering(float Steer)
    {
        foreach (var wheel in Wheels)
            wheel.MyCollider.steerAngle = Steer * MaxAngle;
    }

    public override void UpdateWheelsTransforms()
    {
        foreach (var wheel in Wheels)
            wheel.UpdateWheelTransforms();
    }
}
