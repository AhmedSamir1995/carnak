﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class VehicleController : MonoBehaviour, IVehicleController
{

    public AudioSource Crash;
    //public AudioClip CrashSound;
    public AudioSource Background;
    //public AudioClip BackgroundSound;
    public Vehicle Controlled;
    public MotionVectorsInput Input;
    public bool Networked;
    public VehicleController(Vehicle VehicleToControl, MotionVectorsInput Input)
    {
        ControlledVehicle = VehicleToControl;
        this.Input = Controlled.TorqueSteeringBrake;
    }

    public Vehicle ControlledVehicle
    {
        get
        {
            return Controlled;
        }
        set
        {
            Controlled = value;
        }
    }

    public MotionVectorsInput InputType
    {
        get
        {
            return Input;
        }

        set
        {
            Input = value;
        }
    }

    private void Start()
    {
        if(Networked)
        if (!GetComponent<NetworkIdentity>().isLocalPlayer)
            GetComponent<Car>().cams.gameObject.SetActive(false);
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
            Input = new KeyboardInput();
        else
            Input = new GearVRInput();
            //Input = new MobileInput();
        Controlled.TorqueSteeringBrake = Input;
        GameObject Temp = new GameObject();
        Temp.transform.SetParent(transform);
        if (Background)
        {
            if (!Networked)
                Background.Play();
            else
            {
                if (GetComponent<NetworkIdentity>().isLocalPlayer)
                    Background.Play();
            }
        }
        //Crash = Temp.AddComponent<AudioSource>();
        //Crash = GetComponent<AudioSource>();
      /*  if(!Crash)
            Crash = new AudioSource();*/
        //print(1/GetComponent<NetworkTransform>().sendInterval+" SendRate");
    }
    // Update is called once per frame
    public void Update()
    {
        if (Networked)
        {
            //print("Is Local Player " + GetComponent<NetworkIdentity>().isLocalPlayer);
            if (!GetComponent<NetworkIdentity>().isLocalPlayer)
                return;
        }
        Input.getInput();
        Controlled.Move();
        if(OVRInput.GetDown(OVRInput.Button.Back)||UnityEngine.Input.GetKeyDown(KeyCode.Q))
        {
            if (NetworkClient.allClients.Count == 1)
            {
                foreach (var item in NetworkClient.allClients)
                {
                    item.Disconnect();
                }
                print("Disconnected");
            }

            Destroy(MyNetworkManager.singleton.gameObject);
            MyNetworkManager.Shutdown();
            //UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!Networked)
        {
            if (Crash)
            {
                Crash.loop = false;
                Crash.transform.position = collision.contacts[0].point;
                if (!Crash.isPlaying)
                    Crash.Play();
            }
        }
    }

}
