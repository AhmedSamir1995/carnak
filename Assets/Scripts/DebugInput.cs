﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugInput : MonoBehaviour {
    public UnityEngine.UI.Text Text;
	// Use this for initialization
	void Start () {
        Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
        //Clear();
        //Text.text = OVRInput.Get(OVRInput.Axis2D.PrimaryTouchpad).ToString();
        //Clear();
	}
    public static DebugInput Instance;
    public void Debug(string S)
    {
        
        Text.text = S;
    }
    public void Clear()
    {
        Text.text = "";
    }
}
