﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {
    public Transform ThirdPerson;
    public Transform FirstPerson;
    public UnityEngine.Networking.NetworkIdentity Id;
    public enum FirstOrThird
    {
        FirstPerson,
        ThirdPerson,
        Platformdependant
    }

    public FirstOrThird _FirstOrThird;
	// Use this for initialization
	void Start () {
        if (Id && !Id.isLocalPlayer)
            return;
        if (_FirstOrThird == FirstOrThird.FirstPerson)
        {
            ThirdPerson.gameObject.SetActive(false);
            FirstPerson.gameObject.SetActive(true);
        }
        if (_FirstOrThird == FirstOrThird.ThirdPerson)
        {
            ThirdPerson.gameObject.SetActive(true);
            FirstPerson.gameObject.SetActive(false);
        }
        if (_FirstOrThird == FirstOrThird.Platformdependant)
        {
            if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
            {
                ThirdPerson.gameObject.SetActive(true);
                FirstPerson.gameObject.SetActive(false);
            }
            if (Application.platform == RuntimePlatform.Android)
            {
                ThirdPerson.gameObject.SetActive(false);
                FirstPerson.gameObject.SetActive(true);
            }

        }
    }
	
	// Update is called once per frame
	void Update () {



    }
}
