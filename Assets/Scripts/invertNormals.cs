﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class invertNormals : MonoBehaviour {
    public bool isInverted = false;
    private void InvertNormals()
    {
        print(GetComponent<MeshFilter>().sharedMesh.GetIndexCount(0));
        
        List<Vector3> Normals = new List<Vector3>(GetComponent<MeshFilter>().sharedMesh.normals);
        for (int i = 0; i < Normals.Count; i++)
        {
            Normals[i] = -Normals[i];
        }
        List<int> Indeces = new List<int>(GetComponent<MeshFilter>().sharedMesh.GetIndices(0));
        int temp;
        for (int i = 0; i < Indeces.Count/3; i++)
        {
            //Indeces[i * 3] = -Indeces[i];
            temp = Indeces[i * 3 + 1];
            Indeces[i * 3 + 1] = Indeces[i * 3 + 2];
            Indeces[i * 3 + 2] = temp;
        }

        GetComponent<MeshFilter>().sharedMesh.SetNormals(Normals);
        GetComponent<MeshFilter>().sharedMesh.SetIndices(Indeces.ToArray(), GetComponent<MeshFilter>().sharedMesh.GetTopology(0),0);

        GetComponent<MeshFilter>().sharedMesh.RecalculateBounds();
    }
    // Use this for initialization
    private void Awake()
    {
        InvertNormals();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
