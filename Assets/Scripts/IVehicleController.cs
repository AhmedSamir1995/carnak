﻿public interface IVehicleController
{
    Vehicle ControlledVehicle { get; set; }
    MotionVectorsInput InputType { get; set; }
    void Update();

}