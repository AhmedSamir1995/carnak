﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class GetDistanceTo : MonoBehaviour {
    public Transform OtherTransform;
    public float DistanceToOther;
    public enum WithRespectToAxis
    {
        X,Y,Z,XY,XZ,YZ,XYZ
    }

    public WithRespectToAxis WhatAxis;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        DistanceToOther = CalculateDistance();
	}
    Vector3 MyPosition;
    Vector3 OtherPosition;

    float CalculateDistance()
    {
        MyPosition = transform.position;
        OtherPosition = OtherTransform.position;

        switch (WhatAxis)
        {
            case WithRespectToAxis.X:
                MyPosition.y = 0;
                MyPosition.z = 0;
                OtherPosition.y = 0;
                OtherPosition.z = 0;
                break;
            case WithRespectToAxis.Y:
                MyPosition.x = 0;
                MyPosition.z = 0;
                OtherPosition.x = 0;
                OtherPosition.z = 0;
                break;
            case WithRespectToAxis.Z:
                MyPosition.x = 0;
                MyPosition.y = 0;
                OtherPosition.x = 0;
                OtherPosition.y = 0;
                break;
            case WithRespectToAxis.XY:
                MyPosition.z = 0;
                OtherPosition.z = 0;
                break;
            case WithRespectToAxis.XZ:
                MyPosition.y = 0;
                OtherPosition.y = 0;
                break;
            case WithRespectToAxis.YZ:
                MyPosition.x = 0;
                OtherPosition.x = 0;
                break;
            case WithRespectToAxis.XYZ:
                break;
            default:
                break;
        }
        return Vector3.Distance(MyPosition, OtherPosition);
    }

    public static float CalculateDistance(Transform transform, Transform OtherTransform, WithRespectToAxis WhatAxis)
    {
        Vector3 MyPosition = transform.position;
        Vector3 OtherPosition = OtherTransform.position;

        switch (WhatAxis)
        {
            case WithRespectToAxis.X:
                MyPosition.y = 0;
                MyPosition.z = 0;
                OtherPosition.y = 0;
                OtherPosition.z = 0;
                break;
            case WithRespectToAxis.Y:
                MyPosition.x = 0;
                MyPosition.z = 0;
                OtherPosition.x = 0;
                OtherPosition.z = 0;
                break;
            case WithRespectToAxis.Z:
                MyPosition.x = 0;
                MyPosition.y = 0;
                OtherPosition.x = 0;
                OtherPosition.y = 0;
                break;
            case WithRespectToAxis.XY:
                MyPosition.z = 0;
                OtherPosition.z = 0;
                break;
            case WithRespectToAxis.XZ:
                MyPosition.y = 0;
                OtherPosition.y = 0;
                break;
            case WithRespectToAxis.YZ:
                MyPosition.x = 0;
                OtherPosition.x = 0;
                break;
            case WithRespectToAxis.XYZ:
                break;
            default:
                break;
        }
        return Vector3.Distance(MyPosition, OtherPosition);
    }
}
