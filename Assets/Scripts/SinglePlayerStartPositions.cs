﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinglePlayerStartPositions : MonoBehaviour {
    public GameObject[] startPositions;
    int i = 0;

    static SinglePlayerStartPositions instance;
    public static SinglePlayerStartPositions Instance
    {
        get
        {
            if (instance == null)
                Debug.LogError("SinglePlayerStartPosition singleton not set");
            return instance;
        }
    }
	// Use this for initialization
	void Start () {
        instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public Transform GetRandomStartPosition
    {
        get
        {
            if (ValidateArray)
            {
                Random.InitState((int)Time.frameCount);
                int index = Random.Range(0, startPositions.Length);
                if (ValidateArrayItem(index))
                    return startPositions[index].transform;
            }

                return null;

        }
    }
    public Transform GetRoundRobinStartPosition
    {
        get
        {
            if(ValidateArray)
            {
                i++;
                i %= startPositions.Length;
                int index =i;
                if(ValidateArrayItem(index))
                return startPositions[index].transform;
                
            }

                return null;
        }
    }

    public bool ValidateArray
    {
        get
        {
            if (startPositions.Length <= 0)
            {
                Debug.LogError("StartPositionsArray is Empty");
                return false;
            }
            
            return true;
        }
    }
    bool ValidateArrayItem(int i)
    {
        if (startPositions[i] == null)
        {
            Debug.LogError("Element "+i+" in startPositions is null");
            return false;
        }
        else
            return true;
    }
}
