﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class NetworkUI : MonoBehaviour {
    public NetworkManager M;
    public int PlayerIndex = 0;
    public GameObject[] SinglePlayerCar;
    public UnityEngine.UI.InputField text;
    public string IP;
    public int OfflineSceneIndex;

    [ContextMenu("Do Something")]
    public void DoSomething()
    {
        OfflineSceneIndex = 100;
    }
    // Use this for initialization
    public void StartHost()
    {
        M.StartHost();
    }

    public void StartClient()
    {
        M.networkAddress = IP;
        M.StartClient();
    }

    public void StartServer()
    {
        M.StartServer();
    }

    public void MatchMaking()
    {
        M.StartMatchMaker();
        //M.matchMaker.CreateMatch("asd",4,)
    }

    public void MatchMakingJoinMatch(MatchInfoSnapshot matchInfoSnapshot)
    {
        
        M.matchMaker.JoinMatch(matchInfoSnapshot.networkId, "", Network.player.externalIP, Network.player.ipAddress, 0, 1, OnRespond);
    }
    public void MatchMakingCreateMatch(string name,uint size)
    {
        M.matchMaker.CreateMatch(name, size, true, "", Network.player.externalIP, Network.player.ipAddress, 0, 1, OnRespond);
    }
    void OnRespond(bool success, string extendedInfo, MatchInfo matchInfo)
    {

    }
    public void SinglePlayer()
    {
        AsyncOperation loadLevel = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(OfflineSceneIndex);
        StartCoroutine(LoadCar(loadLevel));
    }

    public void Multiplayer()
    {
        
    }

    public void Quit()
    {
        Application.Quit();
    }

    IEnumerator LoadCar( AsyncOperation loadLevel)
    {
        yield return new WaitUntil(()=>loadLevel.isDone==true);
        GameObject temp = GameObject.Instantiate(SinglePlayerCar[PlayerIndex]);
        Transform SP = SinglePlayerStartPositions.Instance.GetRandomStartPosition;
        temp.transform.position = SP.position;
        temp.transform.rotation = SP.rotation;
    }
}
