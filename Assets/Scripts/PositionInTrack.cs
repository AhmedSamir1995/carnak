﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
[RequireComponent(typeof(NetworkIdentity))]
public class PositionInTrack : NetworkBehaviour
{
    public int TotalLaps;
    public int Lap=0;
    int Checkpoint;
    public UnityEngine.UI.Text rankIndicator;
    float DistanceToLastCheckpoint
    {
        get
        {
            return GetDistanceTo.CalculateDistance(transform, Last.transform, GetDistanceTo.WithRespectToAxis.XZ);
        }
    }
    float DistanceToNextCheckpoint
    {
        get
        {
            return GetDistanceTo.CalculateDistance(transform, Next.transform, GetDistanceTo.WithRespectToAxis.XZ);
        }
    }
    public CheckPoint last;
    public CheckPoint next;
    public CheckPoint Last
    {
        private set
        {
            last = value;
        }
        get
        {
            return last;
        }
    }
    public CheckPoint Next
    {
        set
        {
            

            Last = next;

            if(next.IsFinal)
            {
                Lap++;
                //if (Lap == TotalLaps)
                //    RpcWin();
                Checkpoint = 0;
            }
            else
                Checkpoint++;
            next = value;
            if (isLocalPlayer)
            {
                Last.Highlighted = false;
                Next.Highlighted = true;
            }
        }
        get
        {
            return next;
        }
    }
    public float Position
    {
        get
        {
            return Lap * 100000 +Mathf.Clamp(Checkpoint,0,99) * 1000 + Mathf.Clamp(DistanceToLastCheckpoint,0,999);
        }
    }
    private void Start()
    {
        CheckPoint[] Cs = GameObject.FindObjectsOfType<CheckPoint>();
        foreach (CheckPoint CP in Cs)
        {
            if (CP.IsFinal)
            {
                last = CP;
                next = last.Next;
                next.highlighted = true;
            }
        }
        if (!isLocalPlayer)
        {
            return;
        }

        CmdAddPlayerToRanker();

    }
    [SyncVar]
    public int rank;
    
    [ClientRpc]
    public void RpcWin()
    {
        print("Win\nWin\nWin\nWin\nWin\nWin\nWin\nWin\nWin\nWin\nWin\nWin\nWin\nWin\nWin\nWin\n");
        rankIndicator.text = "You Win";
    }
    [ClientRpc]
    public void RpcLose()
    {
        print("Lose\nLose\nLose\nLose\nLose\nLose\nLose\nLose\nLose\nLose\nLose\nLose\nLose\nLose\n");

    }
    [ClientRpc]
    public void RpcSetRank(int rank)
    {
            this.rank = rank;if(isLocalPlayer)
            if(rankIndicator)
            this.rankIndicator.text = "Your Rank: " + rank;
    }

    [Command]
    void CmdAddPlayerToRanker()
    {
        Ranker.RankerInstance.Players.Add(this);
    }

    [Command]
    void CmdRemovePlayerFromRanker()
    {
        Ranker.RankerInstance.Players.Remove(this);
    }
    private void OnDisconnectedFromServer(NetworkDisconnection info)
    {
        CmdRemovePlayerFromRanker();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.End))
        {
            print("Leaving");
            ClientScene.RemovePlayer(this.playerControllerId);
            Network.Disconnect();
            MasterServer.UnregisterHost();
            //NetworkServer.DestroyPlayersForConnection(this.connectionToClient);
        }
    }
    private void OnPlayerDisconnected(NetworkPlayer player)
    {
        Debug.Log("Clean up after player " + player);
        Network.RemoveRPCs(player);
        Network.DestroyPlayerObjects(player);
        
    }

    

}
