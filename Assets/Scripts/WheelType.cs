﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(WheelCollider))]
public class WheelType : MonoBehaviour
{
    Transform Child;
    public enum FrontOrBack
    {
        Front,
        Back
    }

    public enum LeftOrRight
    {
        Left,
        Right
    }

    public FrontOrBack frontOrBack;
    public LeftOrRight leftOrRight;
    [HideInInspector]
    public WheelCollider MyCollider;

    private void Start()
    {
        MyCollider = GetComponent<WheelCollider>();
        if(MyCollider.transform.childCount>0)
        Child = MyCollider.transform.GetChild(0);
        WheelHit H;
        MyCollider.GetGroundHit(out H);
        
    }

    public void UpdateWheelTransforms()
    {
        Vector3 Position;
        Quaternion Rotation;
        MyCollider.GetWorldPose(out Position, out Rotation);
        if (Child)
        {
            Child.position = Position;
            Child.rotation = Rotation;
        }
        //MyCollider.transform.GetChild(0).transform.position = Position;
        //MyCollider.transform.GetChild(0).transform.rotation = Rotation;
    }
}
