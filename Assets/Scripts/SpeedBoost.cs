﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBoost : MonoBehaviour, ICollectible {
    public float SpeedMultiplier;
    public float TorqueMultiplier;
    public float duration;
    float oldMaxSpeed;
    float oldTorque;
    public void ApplyCollectibleEffect()
    {
        throw new NotImplementedException();
    }

    public void OnTriggerEnter(Collider other)
    {
        Car collidedPlayer= other.transform.parent.parent.gameObject.GetComponent<Car>();
        if(collidedPlayer)
        {
            oldMaxSpeed = collidedPlayer.MaxSpeed;
            collidedPlayer.MaxSpeed *= SpeedMultiplier;
            switch (collidedPlayer.TracktionType)
            {
                case Vehicle.Traction.FourWheelDrive:
                    oldTorque = collidedPlayer.Front.MaxTorque;
                    collidedPlayer.Front.MaxTorque *= TorqueMultiplier;
                    collidedPlayer.Rear.MaxTorque *= TorqueMultiplier;
                    break;
                case Vehicle.Traction.FrontWheelDrive:
                    oldTorque = collidedPlayer.Front.MaxTorque;
                    collidedPlayer.Front.MaxTorque *= TorqueMultiplier;
                    break;
                case Vehicle.Traction.RearWheelDrive:
                    oldTorque = collidedPlayer.Rear.MaxTorque;
                    collidedPlayer.Rear.MaxTorque *= TorqueMultiplier;
                    break;

                default:
                    break;
            }
            StartCoroutine(resetSpeed(collidedPlayer));
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            gameObject.GetComponent<Collider>().enabled = false;
        }
    }
    IEnumerator resetSpeed(Car collidedPlayer)
    {
        Debug.Log("Collected Speed");
        yield return new WaitForSeconds(duration);
        Debug.Log("Deactivating Speed");
        collidedPlayer.MaxSpeed = oldMaxSpeed;
        switch (collidedPlayer.TracktionType)
        {
            case Vehicle.Traction.FourWheelDrive:
                //oldTorque = collidedPlayer.Front.MaxTorque;
                collidedPlayer.Front.MaxTorque = oldTorque;
                collidedPlayer.Rear.MaxTorque = oldTorque;
                break;
            case Vehicle.Traction.FrontWheelDrive:
                //oldTorque = collidedPlayer.Front.MaxTorque;
                collidedPlayer.Front.MaxTorque = oldTorque;
                break;
            case Vehicle.Traction.RearWheelDrive:
                //oldTorque = collidedPlayer.Rear.MaxTorque;
                collidedPlayer.Rear.MaxTorque = oldTorque;
                break;

            default:
                break;
        }
        Destroy(gameObject);

    }

}
