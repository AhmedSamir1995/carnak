﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Car Input/Accelerometer")]
public class MobileInput : MotionVectorsInput
{

    public override void getInput()
    {


        float acceleration = 0;
        float Steer = 0;
        //Debug.Log(Input.acceleration);
        //if(Input.touchCount>0)
        //{
        //    Touch T = Input.GetTouch(0);
        //    //Debug.Log(T.position);
        //    if (T.position.x / Screen.width > 0.8f)
        //        Steer = 1;
        //    else
        //        if (T.position.x / Screen.width < 0.2f)
        //        Steer = -1;
        //    if (T.position.y / Screen.height < 0.3f)
        //        acceleration = -1;
        //    else
        //        if (T.position.y / Screen.height > 0.3f)
        //        acceleration = 1;
        //    Debug.Log(T.position.x / Screen.width + ", " + T.position.y / Screen.height);

        //    //TorqueAndSteering = T.position;
        //}
        Steer= Input.acceleration.x;
        acceleration = -Input.acceleration.z;
            TorqueAndSteering = new Vector2(acceleration, Steer);
        DebugInput.Instance.Debug(TorqueAndSteering + " " + Brake);
    }
}
