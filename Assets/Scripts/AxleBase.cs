﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public abstract class AxleBase {


    public bool Torque;
    public bool Steer;
    public bool HandBrake;
    public bool PedalBrake;
    public float MaxTorque;
    [Range(-50,50)]
    public float MaxAngle;
    [Range(0,float.MaxValue)]
    public float BrakingTorque;
    public bool invertTorque;

    public abstract void Move(MotionVectorsInput TorqueAndSteer);

    protected abstract void CheckHandBrakes(bool Brake);
    protected abstract void CheckPedalBrakes(float Torque);
    protected abstract void CheckTorque(float Torque);
    protected abstract void CheckSteering(float Steer);

    public abstract void UpdateWheelsTransforms();
}
