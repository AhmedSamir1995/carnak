﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;
[RequireComponent(typeof(NetworkManager))]
public class MatchMakingUI : MonoBehaviour {
    NetworkManager networkManager;
    public GameObject uiCanvas;
    public Text matchName;
    public Toggle isPrivate;
    public Toggle isPasswordProtected;
    public Slider noOfPlayers;
    public Text noOfPlayersText;
    public Text matchPassword;
    public Toggle advertiseForMatch;
    public int matchScore;
    public int matchDomain;
    public GameObject failedPanel;
    public GameObject connectingPanel;
    public GameObject matchMakingStuff;
    public GameObject startMatchMakingButton;
    bool aquiringList;
    // Use this for initialization
    void Start() {
        networkManager = GetComponent<NetworkManager>();
    }
    public void setNoOfPlayersText()
    {
        noOfPlayersText.text = noOfPlayers.value.ToString();
    }
    // Update is called once per frame
    void Update() {

    }

    public void StartMatchMaking()
    {
        networkManager.StartMatchMaker();
        //networkManager.matchMaker.ListMatches(0, 30, "", false, 0, 0, OnMatchMakerList);
        ListMatches();
        connectingPanel.SetActive(true);
    }
    public void ListMatches()
    {
        aquiringList = true;
        networkManager.matchMaker.ListMatches(0, 30, "", false, 0, 0, OnMatchMakerList);
    }
    void OnMatchMakerList(bool success, string extendedInfo, List<MatchInfoSnapshot> responseData)
    {
            connectingPanel.SetActive(false);
        if(success)
        {

            matchMakingStuff.SetActive(true);
        }
        else
        {
            showConnectionFailed();
            startMatchMakingButton.SetActive(true);
        }
        networkManager.OnMatchList(success, extendedInfo, responseData);
        print("Success: " + success);
        print("Extended: " + extendedInfo);
        print("MatchInfo: " + networkManager.matches.Count);
        aquiringList = false;
    }
    public void StopMatchMaking()
    {
        networkManager.StopMatchMaker();
    }
    public void ShowMatches()
    {
        networkManager.matchMaker.ListMatches(0, 30, "", false, 0, 0, OnMatchMakerList);
        for (int i = 0; i < networkManager.matches.Count; i++)
        {
            print("Match " + i + " " + networkManager.matches[i].name);
        }
    }
    void showConnectionFailed()
    {
        failedPanel.SetActive(true);
        matchMakingStuff.SetActive(false);
        StopMatchMaking();
    }
    public void CreateMatch()
    {
        networkManager.matchMaker.CreateMatch(matchName.text, (uint)noOfPlayers.value, advertiseForMatch.isOn, matchPassword.text, Network.player.externalIP, Network.player.ipAddress, matchScore, matchDomain, OnMatchCreate);
    }
    public void CreateMatch(string matchName,uint noOfPlayers,int matchScore,int matchDomain)
    {
        networkManager.matchMaker.CreateMatch(matchName, noOfPlayers, true, "", Network.player.externalIP, Network.player.ipAddress, matchScore, matchDomain, OnMatchCreate);
    }
    void OnMatchCreate(bool success, string extendedInfo, MatchInfo responseData)
    {
        networkManager.OnMatchCreate(success, extendedInfo, responseData);
        print("Success: " + success);
        print("Extended: " + extendedInfo);
        print("MatchInfo: " + responseData);
        uiCanvas.SetActive(false);
    }

    public bool JoinMatch(int i)
    {

        networkManager.matchMaker.ListMatches(0, 30, "", false, 0, 0, OnMatchMakerList);
        if (networkManager.matches.Count > i)
        {
            MatchInfoSnapshot matchInfo = networkManager.matches[i];

            networkManager.matchMaker.JoinMatch(matchInfo.networkId, "", Network.player.externalIP, Network.player.ipAddress, 0, 0, OnMatchJoin);
            //if(matchInfo.currentSize==matchInfo.maxSize)
            networkManager.matchMaker.DestroyMatch(matchInfo.networkId, 0, networkManager.OnDestroyMatch);
            return true;
        }
        else
        {
            return false;
        }
    }
    bool MatchListReady()
    {
        return !aquiringList;
    }
    void OnMatchJoin(bool success, string extendedInfo, MatchInfo responseData)
    {
        networkManager.OnMatchJoined(success, extendedInfo, responseData);
        uiCanvas.SetActive(false);
        
    }
    public IEnumerator JoinAnyOrCreateNewMatch()
    {
        ListMatches();
        yield return new WaitUntil(MatchListReady);
        if (JoinMatch(0))
        {
            print("Joining match[0]...");
        }
        else
        {
            print("Creating new match and joining it");
            CreateMatch("TestMatch", 3, 0, 0);
        }

    }
    public void JoinAnyOrCreateNew()
    {
        /*if(JoinMatch(0))
        {
            print("Joining match[0]...");
        }
        else
        {
            print("Creating new match and joining it");
            CreateMatch("TestMatch",3,0,0);
        }*/
        StartCoroutine(JoinAnyOrCreateNewMatch());
    }


}
