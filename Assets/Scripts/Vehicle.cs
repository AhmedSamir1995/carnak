﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public abstract class Vehicle : MonoBehaviour {
    public Transform cams;
    public float MaxSpeed;
    public enum Traction
    {
        FourWheelDrive,
        FrontWheelDrive,
        RearWheelDrive
    }
    protected Rigidbody AttachedRigidBody;
    
    public float Speed;
    public Traction TracktionType;
    public MotionVectorsInput TorqueSteeringBrake;

    protected virtual void Start()
    {

        AttachedRigidBody = GetComponent<Rigidbody>();
        AttachedRigidBody.centerOfMass = new Vector3(0, -.3f, -.5f);
        //Debug.Log(TorqueSteeringBrake);
    }
    
    private void Update()
    {
        //TorqueSteeringBrake.getInput();

        
        //Move();
        //UpdateWheelsTransforms();
        CapSpeed();
        Speed = AttachedRigidBody.velocity.magnitude * 60 * 60 / 1000;
        AttachedRigidBody.AddForce(Vector3.down * Speed);
        //DebugInput.Instance.Debug("Speed= " +((int)Speed).ToString()+"\n");
    }

    protected abstract void UpdateWheelsTransforms();

    public abstract void Move();
    protected abstract void CapSpeed();
}

