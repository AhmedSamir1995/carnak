﻿using System.Collections;
using UnityEditor;
using UnityEngine;

public class ItemUtility
{

    [MenuItem("Assets/Create/Collectibles/Item")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<Item>();
    }

}
