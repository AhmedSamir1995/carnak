﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelsDriver : MonoBehaviour
{
    public enum Traction
    {
        FourWheelDrive,
        FrontWheelDrive,
        RearWheelDrive
    }
    public float MaxTorque;
    public float MaxAngle;
    public WheelCollider[] Wheels=new WheelCollider[4];
    public GameObject WheelPrefab;
    public float BrakingTorque;
    public float Speed;
    public bool invertTorque;
    public Traction TracktionType;
    public Axle Front;
    public Axle Rear;
    MotionVectorsInput TorqueSteeringBrake;
    Rigidbody AttachedRigidBody;
    private void Start()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor)
            TorqueSteeringBrake = new KeyboardInput();
        else
            TorqueSteeringBrake = new GearVRInput();
        //Wheels = GetComponentsInChildren<WheelCollider>();
        for (int i = 0; i < Wheels.Length; i++)
        {
            if (Wheels[i].transform.childCount != 0)
                continue;
            GameObject Wheel = GameObject.Instantiate(WheelPrefab, Wheels[i].transform);
            Wheel.transform.localPosition = Vector3.zero;
        }
        AttachedRigidBody = Wheels[0].attachedRigidbody;
        AttachedRigidBody.centerOfMass = new Vector3(0, -.3f, -.5f);
        print(AttachedRigidBody.centerOfMass);
        Front = new Axle();
        Rear = new Axle();
        /*
        #region //Front
        Front.BrakingTorque = BrakingTorque;
        Front.MaxAngle = MaxAngle;
        Front.MaxTorque = MaxTorque;
        Front.Torque = TracktionType == Traction.FourWheelDrive || TracktionType == Traction.FrontWheelDrive ? true : false;
        Front.Steer = true;
        Front.HandBrake = false;
        Front.invertTorque = invertTorque;
        Front.Left = Wheels[0].gameObject.GetComponent<WheelType>();
        Front.Right = Wheels[1].gameObject.GetComponent<WheelType>();
        Front.PedalBrake = true;
        #endregion
        #region //Rear
        Rear.BrakingTorque = BrakingTorque;
        Rear.MaxAngle = MaxAngle;
        Rear.MaxTorque = MaxTorque;
        Rear.Torque = TracktionType == Traction.FourWheelDrive || TracktionType == Traction.RearWheelDrive ? true : false;
        Rear.Steer = false;
        Rear.HandBrake = true;
        Rear.invertTorque = invertTorque;
        Rear.Left = Wheels[2].gameObject.GetComponent<WheelType>();
        Rear.Right = Wheels[3].gameObject.GetComponent<WheelType>();
        #endregion
        */
    }

    private void Update()
    {
        TorqueSteeringBrake.getInput();
        Move(TorqueSteeringBrake);
        UpdateWheelsTransforms();
        Speed = AttachedRigidBody.velocity.magnitude*60*60/1000;
    }


    void UpdateWheelsTransforms()
    {
        Front.UpdateWheelsTransforms();
        Rear.UpdateWheelsTransforms();
        #region //adim
        /*Vector3 Position;
        Quaternion Rotation;
        for (int i = 0; i < Wheels.Length; i++)
        {

            Wheels[i].GetWorldPose(out Position, out Rotation);
            Wheels[i].transform.GetChild(0).transform.position = Position;
            Wheels[i].transform.GetChild(0).transform.rotation = Rotation;
        }*/
#endregion
    }

    void Move(MotionVectorsInput TorqueAndSteer)
    {

        Front.Move(TorqueSteeringBrake);
        Rear.Move(TorqueSteeringBrake);
        //print("sda"+TorqueAndSteer.TorqueAndSteering.y * MaxAngle);
        /*for (int i = 0; i < Wheels.Length; i++)
        {
            if (i < 2)//Front
            {
                if (TracktionType == Traction.FourWheelDrive || TracktionType == Traction.FrontWheelDrive)
                    Wheels[i].motorTorque = TorqueAndSteer.TorqueAndSteering.x * MaxTorque * (invertTorque ? -1 : 1);
                Wheels[i].steerAngle = TorqueAndSteer.TorqueAndSteering.y * MaxAngle;
            }
            else//Back
            {
                if (TorqueAndSteer.Brake)
                    Wheels[i].brakeTorque = BrakingTorque;
                else
                    Wheels[i].brakeTorque = 0;
                if (TracktionType == Traction.FourWheelDrive || TracktionType == Traction.RearWheelDrive)
                    Wheels[i].motorTorque = TorqueAndSteer.TorqueAndSteering.x * MaxTorque * (invertTorque ? -1 : 1);
            }

        }*/
    }
}
