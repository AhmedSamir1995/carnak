﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSShow : MonoBehaviour
{
    public float FPS;
    public float Frames = 0;
    public float time = 0;
    public float interval = 0.1f;
    public float timeOfLastFrame;
    public UnityEngine.UI.Text Text;
    // Use this for initialization
    void Start()
    {
        Text = GetComponent<UnityEngine.UI.Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Text)
        {
            if (interval <= 0)
                return;
            if (time < interval)
            {
                timeOfLastFrame = Time.deltaTime;
                time += timeOfLastFrame;
                Frames++;
            }
            else
            {

                FPS = Frames / time;
                time = 0;
                Frames = 0;

            }
            Text.text = "FPS " + FPS.ToString();
        }
        //FPS = 1 / Time.deltaTime;
    }
}
