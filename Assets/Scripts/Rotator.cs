﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {
    public float RotationSpeed;
	// Use this for initialization
	void Start () {
		
	}
    Vector3 CurrentRot;
	// Update is called once per frame
	void Update () {
        CurrentRot = transform.rotation.eulerAngles;
        CurrentRot.y += RotationSpeed * Time.deltaTime;
        transform.rotation = Quaternion.Euler(CurrentRot);
	}
}
