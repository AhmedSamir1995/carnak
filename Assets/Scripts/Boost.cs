﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Boost : MonoBehaviour {
    public Rigidbody AttachedRigidbody;
    public Boost(Rigidbody AttachedRigidbody)
    {
        this.AttachedRigidbody=AttachedRigidbody;
    }
    protected abstract void Update();
}
