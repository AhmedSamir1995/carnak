﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ICollectible {

    void OnTriggerEnter(Collider other);

    void ApplyCollectibleEffect();

}
