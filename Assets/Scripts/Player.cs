﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
public class Player : MonoBehaviour {
    public int golds;
    public float experience;
    public List<int> Stats;
	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(Application.dataPath);
	}

    public void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();

        PlayerData pData = new PlayerData()
        {
            Experience = 9,
            Golds = 9000,
            Stats = new List<int>() { 1,3,2}
        };
        Saver.Save<PlayerData>(pData, "PlayerData.dat");
    }
    public void Load( )
    {
        PlayerData pData;
        Saver.Load<PlayerData>(out pData, "PlayerData.dat");
        Debug.Log(pData);

        experience = pData.Experience;
        golds = pData.Golds;
        Stats = pData.Stats;
    }
    [Serializable]
    public class PlayerData : Saveable
    {
        
        int golds;
        float experience;
        List<int> stats;
        public int Golds
        {
            set
            {
                golds = value;
                //Owner.golds = golds;
            }
            get
            {
                return golds;
            }
        }
        public float Experience
        {
            set
            {
                experience = value;
            }
            get
            {
                return experience;
            }
        }

        public List<int> Stats
        {
            get
            {
                return stats;
            }

            set
            {
                stats = value;
            }
        }
    }
}
