﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class MaterialIdentifier : MonoBehaviour {

    public Material localPlayerMaterial;
    public Material nonLocalPlayerMaterial;
    public NetworkIdentity Player;

    // Use this for initialization
    void Start () {
        if (Player.isLocalPlayer)
        {
            GetComponent<MeshRenderer>().sharedMaterial = localPlayerMaterial;
        }
        else
        {
            GetComponent<MeshRenderer>().sharedMaterial = nonLocalPlayerMaterial;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
