﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class MeshCombiningManager : MonoBehaviour {
    List<MeshRenderer> SceneObjects;
    public List<Material> materials;
    public List<List<Transform>> MaterialsTransforms;
    public int ListsCount;
    public GameObject EmptyCombinedPrefab;
    // Use this for initialization
    void Start () {
        MaterialsTransforms = new List<List<Transform>>();
        SceneObjects = new List<MeshRenderer>(GetSceneObjects<MeshRenderer>());
        materials = new List<Material>(GetMaterials());

	}
	
    Material[] GetMaterials( )
    {
        List<Material> materials = new List<Material>();
        for(int i=0;i<SceneObjects.Count;i++ )
        {

            int materialIndex= materials.FindIndex((x) => { return x == SceneObjects[i].sharedMaterial; });
            if (-1 != materialIndex)
            {
                MaterialsTransforms[materialIndex].Add(SceneObjects[i].transform);
                continue;
            }
            materials.Add(SceneObjects[i].sharedMaterial);
            MaterialsTransforms.Add(new List<Transform>(){SceneObjects[i].transform  });

        }
        ListsCount = MaterialsTransforms.Count;

        for(int i=0;i<MaterialsTransforms.Count;i++)
        {
            GameObject temp = new GameObject("Combiner");
            temp.AddComponent<MyMeshCombiner>().enabled = false;
            temp.GetComponent<MyMeshCombiner>().children=MaterialsTransforms[i].ToArray();
            temp.GetComponent<MyMeshCombiner>().EmptyCombinedPrefab = EmptyCombinedPrefab;
            temp.GetComponent<MyMeshCombiner>().mat = materials[i];
            temp.GetComponent<MyMeshCombiner>().enabled = true;
        }

        return materials.ToArray();
    }

    T[] GetSceneObjects<T>() where T : UnityEngine.Object
    {
        return GameObject.FindObjectsOfType<T>();
    }

    T[] GetStaticSceneObjects<T>() where T : UnityEngine.MonoBehaviour
    {
        T[] SceneObjects = GetSceneObjects<T>();
        List<T> StaticOnly = new List<T>();
        for (int i = 0; i < SceneObjects.Length; i++)
        {
            if (SceneObjects[i].gameObject.isStatic)
                StaticOnly.Add(SceneObjects[i]);
        }
        return StaticOnly.ToArray();
    }
    // Update is called once per frame
    void Update () {
		
	}
}
