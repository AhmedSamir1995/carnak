﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(MeshFilter),typeof(MeshRenderer))]
public class MeshCombiner : MonoBehaviour {
    
    public Transform Target;
    public void Combine()
    {
        Vector3 oldPos = transform.position;
        Quaternion oldRot = transform.rotation;
        transform.rotation = Target.rotation;
        transform.position = Target.position;

        MeshFilter Myfilter = GetComponent<MeshFilter>();
        MeshRenderer MyRenderer = GetComponent<MeshRenderer>();
        List<CombineInstance> meshes = CombineMeshesGenerator();
        Mesh M = new Mesh();
        
        
        //Target.gameObject.SetActive(false);
        transform.rotation = oldRot;
        transform.position = oldPos;
        Mesh finalMesh = new Mesh();
        Debug.Log(meshes.Count);
        finalMesh.CombineMeshes(meshes.ToArray(),false,true);

        MyRenderer.sharedMaterials = MaterialsGenerator(finalMesh);
        Myfilter.mesh = finalMesh;
        //AssetDatabase.CreateAsset(finalMesh, "NewModelTest.asset");
        //AssetDatabase.SaveAssets();
    }

    public List<CombineInstance> CombineMeshesGenerator()
    {

        List<CombineInstance> meshes = new List<CombineInstance>();
        MeshFilter[] Filters = Target.GetComponentsInChildren<MeshFilter>();
        Debug.Log("This contains " + Filters.Length);
        for (int i = 0; i < Filters.Length; i++)
        {
            if (!Filters[i].gameObject.activeInHierarchy)
            {
                Debug.Log("wa2f");
                continue;
            }
            if (Filters[i].sharedMesh)
                for (int j = 0; j < Filters[i].sharedMesh.subMeshCount; j++)
                {
                    //Debug.Log(Filters[i].name + " " + meshes.Count);
                    CombineInstance temp = new CombineInstance();
                    temp.subMeshIndex = j;
                    temp.mesh = Filters[i].sharedMesh;
                    temp.transform = Filters[i].transform.localToWorldMatrix;
                    meshes.Add(temp);
                }
            //if (Filters[i].gameObject != gameObject)
              //  DestroyImmediate(Filters[i].gameObject);
        }
        return meshes;
    }
    public void CombineDifferentMaterials()
    {
        int submesh=0;
        MeshFilter[] Filters = Target.GetComponentsInChildren<MeshFilter>();
        Debug.Log("This contains " + Filters.Length);
        List<Vector3> Vertices = new List<Vector3>();
        List<int[]> Indices = new List<int[]>();
        List<int[]> triangles = new List<int[]>();
        List<int> submeshIndex = new List<int>();
        List<Vector2> UVs = new List<Vector2>();
        List<Material> materials = new List<Material>();
        int lastVertexCount=0;
        for(int i=0;i<Filters.Length;i++)//for each mesh in an meshfilter
        {
            //Debug.Log(Filters[i].name+" "+ Filters[i].sharedMesh.subMeshCount);
            Mesh current = Filters[i].sharedMesh;
            for (int k = 0; k < current.vertices.Length; k++)
            {
                Debug.Log("Old vertices" + current.vertices[k]);
                current.vertices[k] = Filters[i].transform.localToWorldMatrix.MultiplyPoint3x4(current.vertices[k]);
                Debug.Log("New vertices" + current.vertices[k]);
            }
            lastVertexCount = Vertices.Count;
            Vertices.AddRange(current.vertices);
            UVs.AddRange(current.uv);
            for(int j=0;j<current.subMeshCount;j++)//for each submesh
            {
                int[] indeces = current.GetTriangles(j);
                for (int temp = 0; temp < indeces.Length; temp++)
                {
                    indeces[temp] += lastVertexCount;
                }
                    submeshIndex.Add(submesh);
                submesh++;
                
                triangles.Add(indeces);

                
            }
        }

        MeshFilter myFilter = GetComponent<MeshFilter>();
        if (myFilter.sharedMesh == null)
            myFilter.sharedMesh = new Mesh();
        myFilter.sharedMesh.SetVertices(Vertices);
        myFilter.sharedMesh.SetUVs(0,UVs);

        //myFilter.sharedMesh.SetTriangles(triangles[1], 0);
        myFilter.sharedMesh.subMeshCount = submesh;
        for(int i=0;i<triangles.Count;i++)
        {
            myFilter.sharedMesh.SetTriangles(triangles[i], i);
            //Debug.Log("tris " + i);
        }

    }

    public Material[] MaterialsGenerator(Mesh finalMesh)
    {
        MeshRenderer[] Renderers = Target.GetComponentsInChildren<MeshRenderer>();
        Material[] sharedMaterials = new Material[finalMesh.subMeshCount];

        int newRendererMaterialIndex = 0;
        for (int i = 0; i < Renderers.Length; i++)
        {
            for (int j = 0; j < Renderers[i].sharedMaterials.Length; j++)
            {
                Debug.Log("asd");
                sharedMaterials[newRendererMaterialIndex] = new Material(Renderers[i].sharedMaterials[j]);
                newRendererMaterialIndex++;
            }
        }
        return sharedMaterials;
    }
    public void DisableChildren()
    {
        
    }
    
}
