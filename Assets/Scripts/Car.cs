﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : Vehicle
{
    public Axle Front;
    public Axle Rear;
    public Transform SteeringWheel;
    public float SteeringWheelMaxAngle;
    protected override void Start()
    {
        base.Start();
        switch (TracktionType)
        {
            case Traction.FourWheelDrive:
                Front.Torque = true;
                Rear.Torque = true;
                break;
            case Traction.FrontWheelDrive:
                Front.Torque = true;
                break;
            case Traction.RearWheelDrive:
                Rear.Torque = true;
                break;
            default:
                break;
        }
    }
    protected override void CapSpeed()
    {
        float speed = AttachedRigidBody.velocity.magnitude*3.6f;
        if(speed>MaxSpeed)
            AttachedRigidBody.velocity=AttachedRigidBody.velocity.normalized*(MaxSpeed/3.6f);
    }
    public override void Move()
    {
        if(SteeringWheel) RotateSteeringWheel();
        Front.Move(TorqueSteeringBrake);
        Rear.Move(TorqueSteeringBrake);
        UpdateWheelsTransforms();
    }
    void RotateSteeringWheel()
    {

        Vector3 OldRot = SteeringWheel.localRotation.eulerAngles;
        Quaternion NewRot = Quaternion.Euler(Vector3.Lerp(OldRot, new Vector3(OldRot.x, OldRot.y, TorqueSteeringBrake.TorqueAndSteering.y * SteeringWheelMaxAngle),Time.deltaTime*0.1f));
        SteeringWheel.localRotation = Quaternion.Euler(OldRot.x, OldRot.y, TorqueSteeringBrake.TorqueAndSteering.y * SteeringWheelMaxAngle);
    }
    protected override void UpdateWheelsTransforms()
    {
        SteerHelper();
        Front.UpdateWheelsTransforms();
        Rear.UpdateWheelsTransforms();
    }
    public float m_SteerHelper = 1;
    float m_OldRotation = 0;
    private void SteerHelper()
    {
        for (int i = 0; i < 4; i++)
        {
            WheelHit wheelhit;
            if(i<2)
            Rear.Wheels[i].MyCollider.GetGroundHit(out wheelhit);
            else
            Front.Wheels[i-2].MyCollider.GetGroundHit(out wheelhit);
            if (wheelhit.normal == Vector3.zero)
                return; // wheels arent on the ground so dont realign the rigidbody velocity
        }

        // this if is needed to avoid gimbal lock problems that will make the car suddenly shift direction
        if (Mathf.Abs(m_OldRotation - transform.eulerAngles.y) < 10f)
        {
            var turnadjust = (transform.eulerAngles.y - m_OldRotation) * m_SteerHelper;
            Quaternion velRotation = Quaternion.AngleAxis(turnadjust, Vector3.up);
           AttachedRigidBody.velocity = velRotation * AttachedRigidBody.velocity;
        }
        m_OldRotation = transform.eulerAngles.y;
    }


}
