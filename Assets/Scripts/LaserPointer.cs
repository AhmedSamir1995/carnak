﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserPointer : MonoBehaviour {
    public GameObject Laser ;
    LineRenderer lineRenderer;
    public float infinity;
    //Vector3 OriginalScale;
	// Use this for initialization
	void Start () {
        //OriginalScale = Laser.transform.localScale;
        lineRenderer = Laser.GetComponent<LineRenderer>();
	}
    RaycastHit RH;
	
	// Update is called once per frame
	void Update () {
            Vector3[] verts = new Vector3[2];
            verts[0] = transform.position;
        if(Physics.Raycast(this.transform.position, this.transform.forward, out RH))
        {
            verts[1] = RH.point;
            //Laser.transform.localScale = new Vector3(OriginalScale.x, OriginalScale.y, Vector3.Distance(this.transform.position, RH.point));
            //Laser.transform.position = Vector3.Lerp(transform.position, RH.point, 0.5f);
        }
        else
        {
            //Laser.transform.localScale = new Vector3(OriginalScale.x, OriginalScale.y, infinity);

            verts[1] = transform.position+transform.forward*infinity;
            //Laser.transform.position = Vector3.Lerp(transform.position, transform.position+transform.forward* infinity, 0.5f);
        }
            lineRenderer.SetPositions(verts);
	}
}
