﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
public class SyncTransform : NetworkBehaviour {
    [SyncVar]
    Vector3 SyncPos;

    [SyncVar]
    Vector3 WheelsPos;
    [SyncVar]
    Vector3 WheelsPos1;
    [SyncVar]
    Vector3 WheelsPos2;
    [SyncVar]
    Vector3 WheelsPos3;

    [SyncVar]
    Quaternion SyncRot;

    [SyncVar]
    Quaternion WheelsRot;
    [SyncVar]
    Quaternion WheelsRot1;
    [SyncVar]
    Quaternion WheelsRot2;
    [SyncVar]
    Quaternion WheelsRot3;

    [SerializeField] public Transform[] Wheels;
    [SerializeField] Transform MyTransform;
    [SerializeField] float LerpRate = 15;

    private void Start()
    {
        MyTransform = transform;
        SyncRot = transform.rotation;
    }
    private void Update()
    {
        TransmitPosition();
        LerpPosition();
        TransmitRotation();
        LerpRotation();
    }

    void LerpPosition()
    {
        if(!isLocalPlayer)
        {
            MyTransform.position = Vector3.Lerp(MyTransform.position, SyncPos, LerpRate);

            Wheels[0].position = Vector3.Lerp(Wheels[0].position, WheelsPos, LerpRate);
            Wheels[1].position = Vector3.Lerp(Wheels[1].position, WheelsPos1, LerpRate);
            Wheels[2].position = Vector3.Lerp(Wheels[2].position, WheelsPos2, LerpRate);
            Wheels[3].position = Vector3.Lerp(Wheels[3].position, WheelsPos3, LerpRate);

        }
    }
    void LerpRotation()
    {
        if (!isLocalPlayer)
        {
            MyTransform.rotation = Quaternion.Lerp(MyTransform.rotation, SyncRot, LerpRate);

            Wheels[0].rotation = Quaternion.Lerp(Wheels[0].rotation, WheelsRot, LerpRate);
            Wheels[1].rotation = Quaternion.Lerp(Wheels[1].rotation, WheelsRot, LerpRate);
            Wheels[2].rotation = Quaternion.Lerp(Wheels[2].rotation, WheelsRot, LerpRate);
            Wheels[2].rotation = Quaternion.Lerp(Wheels[3].rotation, WheelsRot, LerpRate);

        }
    }

    [Command]
    void CmdProvidePositionToServer(Vector3 pos,Vector3[] wheelsPos)
    {
        SyncPos = pos;

        //MyTransform.position = Vector3.Lerp(MyTransform.position, SyncPos, LerpRate);
        WheelsPos = wheelsPos[0];
        WheelsPos1 = wheelsPos[1];
        WheelsPos2 = wheelsPos[2];
        WheelsPos3 = wheelsPos[3];

    }
    [Command]
    void CmdProvideRotationToServer(Quaternion rot,Quaternion[] wheelsRot)
    {
        SyncRot = rot;
        
            //MyTransform.position = Vector3.Lerp(MyTransform.position, SyncPos, LerpRate);
            WheelsRot = wheelsRot[0];
            WheelsRot1 = wheelsRot[1];
            WheelsRot2 = wheelsRot[2];
            WheelsRot3 = wheelsRot[3];
        
    }

    [ClientCallback]
    void TransmitPosition()
    {
        if (isLocalPlayer)
        {
            Vector3[] Positions=new Vector3[Wheels.Length];
            for (int i = 0; i < Wheels.Length; i++)
                Positions[i] = Wheels[i].position;
            CmdProvidePositionToServer(MyTransform.position,Positions);
        }
    }
    [ClientCallback]
    void TransmitRotation()
    {
        if (isLocalPlayer)
        {
            Quaternion[] Rotations=new Quaternion[Wheels.Length];
            for (int i = 0; i < Wheels.Length; i++)
                Rotations[i] = Wheels[i].rotation;
            CmdProvideRotationToServer(MyTransform.rotation,Rotations);
        }
    }
}
