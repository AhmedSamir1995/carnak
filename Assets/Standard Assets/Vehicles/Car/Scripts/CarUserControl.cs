using System;
using UnityEngine;
namespace UnityStandardAssets.Vehicles.Car
{
    [RequireComponent(typeof (CarController))]
    public class CarUserControl : MonoBehaviour
    {
        public Camera MyCam;
        private CarController m_Car; // the car controller we want to use


        private void Awake()
        {
            // get the car controller
            m_Car = GetComponent<CarController>();
        }

        private void Start()
        {
        }
        private void FixedUpdate()
        {
            
            // pass the input to the car!
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            //#if !MOBILE_INPUT
            //            float handbrake = CrossPlatformInputManager.GetAxis("Jump");
            //            m_Car.Move(h, v, v, handbrake);
            //#else
            //            m_Car.Move(h, v, v, 0f);
            //#endif
            //            print(h + " " + v);
            float HandBrake = Input.GetKey(KeyCode.Space) == true ? 1 : 0;
            m_Car.Move(h, v, v, HandBrake);
        }
    }
}
