﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSwitcher : MonoBehaviour {
    public GameObject A;
    public GameObject B;
    public Vector3 OriginPosition;
    public Quaternion OriginRotation;
    bool AorB=true;
    int i = 0;
	// Use this for initialization
	void Start () {

        Change();
    }
	Coroutine Clearer;
	// Update is called once per frame
	void Update () {
		if(OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger)||Input.GetKeyDown(KeyCode.Space))
        {
            Change();
        }


	}

    IEnumerator ClearCombo()
    {
        yield return new WaitForSeconds(3);
        i = 0;
    }

    private void Change()
    {
        AorB = !AorB;
        A.SetActive(AorB);
        B.SetActive(!AorB);
        if (AorB)
        {
            A.transform.position = transform.position;
            A.transform.rotation = transform.rotation;
        }
        else
        {
            B.transform.position = transform.position;
            B.transform.rotation = transform.rotation;
        }

        i = 0;
    }
}
