﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdjustLocation : MonoBehaviour {
    public GameObject scaleOwner;
    Vector3 originalPosition;
    // Use this for initialization
    void Start () {
        originalPosition=transform.position/transform.parent.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = originalPosition * transform.parent.localScale.x;

        transform.position = new Vector3(transform.position.x * scaleOwner.transform.localScale.x,
        transform.position.y * scaleOwner.transform.localScale.y,
        transform.position.z * scaleOwner.transform.localScale.z);

    }
}
